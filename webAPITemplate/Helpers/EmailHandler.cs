﻿using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace webAPITemplate.Helpers
{
    public interface IEmailHandler
    {
        void SendEmail(List<string> attachments, string subject, string body, string from, string to, string toCc, string toCC2);
    }

    public class EmailHandler : IEmailHandler
    {
        private List<System.Net.Mail.Attachment> _attachmentsToDispose;

        private HttpFileCollection fuAttachment;
        
        public void SendEmail(List<string> attachments, string subject, string body, string from, string to, string toCc, string toCC2)
        {

            var smtpClient = new SmtpClient("smtp.socketlabs.com", Convert.ToInt16(25))
            {
                Credentials = new System.Net.NetworkCredential(
                    "server7022",
                    "Ls94KoWa8r5A")
            };

            var mail = new MailMessage
            {
                From = new MailAddress(from, "www.rushstarwireless.com"),
                IsBodyHtml = true
            };


            try
            {
                string[] sEmails = to.Split(',');

                foreach (string email in sEmails)
                {

                    if (email.Trim().Length > 0)
                        mail.To.Add(new MailAddress(email));
                    //mail.To.Add(new MailAddress(to));
                }

                if (!string.IsNullOrEmpty(toCc))
                    mail.CC.Add(new MailAddress(toCc));

                if (!string.IsNullOrEmpty(toCC2))
                    mail.CC.Add(new MailAddress(toCC2));

                mail.Bcc.Add(new MailAddress("agaray@rushstarwireless.com"));
                mail.Subject = subject;
                mail.Body = body;

                AddAttachments(mail, attachments);

                smtpClient.Send(mail);
            }
            catch (Exception ex)
            {
                //add logging

                throw ex;

            }
            finally
            {
                DisposeAttachmentFiles();
                smtpClient.Dispose();
            }
        }

        public void EmailSendingRR(List<string> attachments, string subject, string body, string from, string to, string toCc, string toCC2)
        {

            using (MailMessage mailMessage = new MailMessage())
            {
                //Email description
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.To.Add(new MailAddress(to));
                string emailDisplayName = "Rush Star Wireless";
                mailMessage.From = new MailAddress(from, emailDisplayName);


                //mailMessage.IsBodyHtml = true;

                //AddAttachments(mailMessage, attachments);

                //Create smtp server and config
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["HostRR"];
                smtp.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSslRR"]);
                System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                NetworkCred.UserName = ConfigurationManager.AppSettings["UserNameRR"];
                NetworkCred.Password = ConfigurationManager.AppSettings["PasswordRR"];
                smtp.Port = int.Parse(ConfigurationManager.AppSettings["PortRR"]);
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;

                // Send mail after configuring.
                smtp.Send(mailMessage);
            }

        }
        
        public async Task<bool> Mailer(string subject, string body, string to, string from, List<string> fileNames)

        {

            // Send Grid and Client creation

            var Localkey = ConfigurationManager.AppSettings["sendGridKey"];

            var apiKey = (Localkey);

            var client = new SendGridClient(Localkey);



            // Build Email

            var SGfrom = new EmailAddress(from, "Rush Star Wireless");

            var SGto = new EmailAddress(to);

            var SGsubject = subject;

            var SGbody = body;

            var msg = MailHelper.CreateSingleEmail(SGfrom, SGto, SGsubject, "", SGbody);

            msg.Attachments = new List<SendGrid.Helpers.Mail.Attachment>();


            //for (int i=0;i<fileNames.Count;i++)
            //{

            //    MemoryStream theMemStream = new MemoryStream();

            //    theMemStream.Write(File.ReadAllBytes(fileNames[i]), 0, fileNames[i].Length);

            //    await AddAttachmentAsync(msg.Attachments, fileNames[i], theMemStream);

            //}


            //string fileName = "";


            for (int i = 0; i < fileNames.Count; i++)
            {

                if (!string.IsNullOrEmpty(fileNames[i]))
                {

                    var bytes = File.ReadAllBytes(fileNames[i]);

                    var file64 = Convert.ToBase64String(bytes);

                    msg.Attachments.Add(AddAttachment(fileNames[i], file64, null, null, null));

                }
            }

            try
            {
                var response = await client.SendEmailAsync(msg);

                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        
        public async Task<bool> SendEmailOriginal( string subject, string body, string from, string to, string toCc, string toCC2)
        {

            var smtpClient = new SmtpClient("smtp.socketlabs.com", Convert.ToInt16(25))
            {
                Credentials = new System.Net.NetworkCredential(
                    "server7022",
                    "Ls94KoWa8r5A")
            };

            var mail = new MailMessage
            {
                From = new MailAddress(from, "www.rushstarwireless.com"),
                IsBodyHtml = true
            };


            try
            {
                string[] sEmails = to.Split(',');

                foreach (string email in sEmails)
                {

                    if (email.Trim().Length > 0)
                    {

                        mail.To.Add(new MailAddress(email));
                    
                        if (!string.IsNullOrEmpty(toCc))
                            mail.CC.Add(new MailAddress(toCc));

                        if (!string.IsNullOrEmpty(toCC2))
                            mail.CC.Add(new MailAddress(toCC2));

                        mail.Subject = subject;
                        mail.Body = body;

                    

                        smtpClient.Send(mail);

                    }


                }

               


                return await Task.Run(() => { return true; });


            }
            catch (Exception ex)
            {
                //add logging

                throw ex;

                

            }
            finally
            {
                DisposeAttachmentFiles();
                smtpClient.Dispose();
            }

            return await Task.Run(() => { return false; });

        }
               
        public async Task<bool> SendEmailEmail(string subject, string body, string to, string from)
        {

            var Localkey = ConfigurationManager.AppSettings["sendGridKey"];

            var apiKey = (Localkey);

            var client = new SendGridClient(Localkey);

            // Build Email

            var SGfrom = new EmailAddress(from, "Rush Star Wireless");

            var SGto = new EmailAddress(to);

            var SGsubject = subject;

            var SGbody = body;

            var msg = MailHelper.CreateSingleEmail(SGfrom, SGto, SGsubject, "", SGbody);

            //msg.Attachments = new List<SendGrid.Helpers.Mail.Attachment>();

            //for (int i=0;i<fileNames.Count;i++)
            //{

            //    MemoryStream theMemStream = new MemoryStream();

            //    theMemStream.Write(File.ReadAllBytes(fileNames[i]), 0, fileNames[i].Length);

            //    await AddAttachmentAsync(msg.Attachments, fileNames[i], theMemStream);

            //}


            //string fileName = "";


            //for (int i = 0; i < fileNames.Count; i++)
            //{

            //    if (!string.IsNullOrEmpty(fileNames[i]))
            //    {

            //        var bytes = File.ReadAllBytes(fileNames[i]);

            //        var file64 = Convert.ToBase64String(bytes);

            //        msg.Attachments.Add(AddAttachment(fileNames[i], file64, null, null, null));

            //    }
            //}

            try
            {
                var response = await client.SendEmailAsync(msg);

                return true;
            }
            catch (Exception ex)
            {

                return false;
            }

        }
               
        private void AddAttachments(MailMessage mailMessage, List<string> attachments)
        {
            if (attachments == null)
                return;

            if (attachments.Count <= 0)
                return;

            if (_attachmentsToDispose == null)
                _attachmentsToDispose = new List<System.Net.Mail.Attachment>();
            else
            {
                _attachmentsToDispose.Clear();
            }

            foreach (var attachment in attachments)
            {
                var emailAttachment = new System.Net.Mail.Attachment(attachment);
                _attachmentsToDispose.Add(emailAttachment);
                mailMessage.Attachments.Add(emailAttachment);
            }

        }

        private void DisposeAttachmentFiles()
        {
            if (_attachmentsToDispose == null)
                return;

            foreach (var attachment in _attachmentsToDispose)
            {
                // attachment.Dispose();
            }

            _attachmentsToDispose.Clear();
        }
        
        private async Task AddAttachmentAsync(List<SendGrid.Helpers.Mail.Attachment> ls, string filename, Stream contentStream, string type = null, string disposition = null, string content_id = null, CancellationToken cancellationToken = default(CancellationToken))
        {
            // Stream doesn't want us to read it, can't do anything else here
            if (contentStream == null || !contentStream.CanRead)
            {
                return;
            }

            var contentLength = Convert.ToInt32(contentStream.Length);
            var streamBytes = new byte[contentLength];

            await contentStream.ReadAsync(streamBytes, 0, contentLength, cancellationToken);

            var base64Content = Convert.ToBase64String(streamBytes);

            ls.Add(AddAttachment(filename, base64Content, type, disposition, content_id));
        }

        private SendGrid.Helpers.Mail.Attachment AddAttachment(string filename, string base64Content, string type = null, string disposition = null, string content_id = null)
        {
            if (string.IsNullOrWhiteSpace(filename) || string.IsNullOrWhiteSpace(base64Content))
            {
                return null;
            }

            var attachment = new SendGrid.Helpers.Mail.Attachment
            {
                Filename = filename,
                Content = base64Content,
                Type = type,
                Disposition = disposition,
                ContentId = content_id
            };

            return attachment;
        }


    }
}