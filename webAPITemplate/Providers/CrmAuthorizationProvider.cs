﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Infraestructure.DL;
using System.Threading.Tasks;
using Microsoft.Owin.Security;
using System.Security.Claims;
//using System.Web.Http.Cors;
using webAPITemplate.Processor;

using Models.Auth;
using System.Web.Http.Cors;
using webAPIeShop.Processor;
using Infreastructure.DL;

namespace webAPITemplate.Providers
{
    public class CrmAuthorizationProvider : OAuthAuthorizationServerProvider
    {

        private IAuthentication _authentication;

        public CrmAuthorizationProvider()
        {
                _authentication = new Authentication();

        }


        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public override async System.Threading.Tasks.Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            await System.Threading.Tasks.Task.Run(() => { return context.Validated(); });
        }

        /// <summary>
        /// Authenticate Username and Password in CRM Dynamics 2011
        /// </summary>
        /// <param name="context">The context in which the username and passaword are stored.</param>
        /// <returns></returns>
        public override async System.Threading.Tasks.Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            string fullName = null;
            string password = null;
            string AccountNumber = null;
            Guid AccountID = new Guid("00000000-0000-0000-0000-000000000000");
            string Warehouse = "";

            Guid systemUserId = new Guid("00000000-0000-0000-0000-000000000000");
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
            bool isAuthenticated = false;
            AuthUser result = new AuthUser();

            try
            {
                await Task.Run(() =>
                {

                    //AuthProcessor authPRoc = new AuthProcessor(new AuthCRUD());


                    //result = authPRoc.GetTokenByUserPass(authUser).Result;

                    //var proc = new UsersProcessors(new CRMUsersCRUD());
                    fullName = context.UserName;  //proc.GetUserByUsername(context.UserName).Result;
                    //systemUserId = proc.GetsystemUserIDByUsername(context.UserName).Result;

                    //var proc = new UserProcessor(new CRMUsersCRUD());
                    //fullName = result.User; //proc.GetUserByUsername(context.UserName).Result;
                    //password = result.Pass; //proc.GetsystemUserIDByUsername(context.UserName).Result;

                    //if (!String.IsNullOrEmpty(result.AccountNumber))
                    //    AccountNumber = result.AccountNumber;

                    //AccountID = result.AccountID;

                    //if(!String.IsNullOrEmpty(result.Warehouse))
                    //    Warehouse = result.Warehouse;
                });

                await Task.Run(() =>
                {
                    isAuthenticated = _authentication.IsAuthenticationOk(context.UserName, context.Password).Result;
                });
            }
            catch (Exception ex)
            {
                context.SetError("invalid_grant", ex.Message + "\r\n" + ex.InnerException?.Message);
                context.Rejected();
                return;
            }

            
            //if(!string.IsNullOrEmpty(result.AccountNumber))
            if(isAuthenticated)
            {

                //{"Password", password.ToString() },
                var props = new AuthenticationProperties(
                    new Dictionary<string, string>
                    {
                      {"UserFullName", fullName } //,
                     // {"systemUserId", systemUserId.ToString() }
                    });
                var ticket = new AuthenticationTicket(identity, props);
                context.Validated(ticket);

                //TokenItem tokItem = new TokenItem();
                //tokItem.AccountID = AccountID;
                //tokItem.Token = "";

                return;
            }

            context.SetError("invalid_grant", "Provided username and password is incorrect");
            context.Rejected();
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
            return Task.FromResult<object>(null);
        }














    }
}