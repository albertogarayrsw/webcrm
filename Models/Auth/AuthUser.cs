﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Models.Auth
{
    public class AuthUser
    {

        public string User { get;  set; }
        public string Pass { get;  set; }
        public string token { get;  set; }
        public string AccountNumber { get; set; }

        public Guid AccountID { get; set; }

        public string Warehouse { get; set; }


    }
}
