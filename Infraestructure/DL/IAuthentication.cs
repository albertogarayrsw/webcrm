﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.DL
{
    public interface IAuthentication
    {

        Task<bool> IsAuthenticationOk(string username, string password);

    }
}
