﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Client;
using Microsoft.Xrm.Client.Services;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.DL
{
    public class Authentication : IAuthentication
    {
        public async  Task<bool> IsAuthenticationOk(string username, string password)
        {
         
            try
            {

                var connection = CrmConnection.Parse("Url=http://" + "10.0.183.203:5555" + "/CRMDB; Domain=RSW; Username=" + username + "; Password = " + password + ";");
                var serviceProxy = new OrganizationService(connection);
                var userid = ((WhoAmIResponse)serviceProxy.Execute(new WhoAmIRequest())).UserId;
                return await Task.Run(() => { return true; });
            }
            catch (Exception ex)
            {
                return await Task.Run(() => { return false; });
            }

        }
    }
}
